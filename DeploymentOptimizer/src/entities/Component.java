package entities;

public class Component {
	
	String ID;
	String name;
	int VMtype; //1=small,2=medium,3=large,4=xlarge,5=2xlarge
	int storageBlocks; //costo per GB
	int dataTransferRate;//9 fasce, vedi report
	String [] requiredControls;
	
	public Component(){
		this.VMtype=0;
		this.storageBlocks=0;
		this.dataTransferRate=0;
	}
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getVMtype() {
		return VMtype;
	}
	public void setVMtype(int vMtype) {
		VMtype = vMtype;
	}
	public int getStorageBlocks() {
		return storageBlocks;
	}
	public void setStorageBlocks(int storageBlocks) {
		this.storageBlocks = storageBlocks;
	}
	public int getDataTransferRate() {
		return dataTransferRate;
	}
	public void setDataTransferRate(int dataTransferRate) {
		this.dataTransferRate = dataTransferRate;
	}

	public String[] getRequiredControls() {
		return requiredControls;
	}

	public void setRequiredControls(String[] requiredControls) {
		this.requiredControls = requiredControls;
	}
	
	
	
}
