package entities;
import java.util.ArrayList;

public class Provider {

	String ID;
	String name;
	float storageBlockCost;
	float transferRateCosts [];
	float VMcosts [];
	String [] offeredControls;
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float [] getTransferRateCosts() {
		return transferRateCosts;
	}
	public void setTransferRateCosts(float transferRateCosts []) {
		this.transferRateCosts = transferRateCosts;
	}
	public float [] getVMcosts() {
		return VMcosts;
	}
	public void setVMcosts(float vMcosts []) {
		VMcosts = vMcosts;
	}
	public void setStorageBlockCost(float storageBlockCost) {
		this.storageBlockCost = storageBlockCost;
	}
	public float getStorageBlockCost() {
		return storageBlockCost;
	}
	public String[] getOfferedControls() {
		return offeredControls;
	}
	public void setOfferedControls(String[] offeredControls) {
		this.offeredControls = offeredControls;
	}
	
	
}
