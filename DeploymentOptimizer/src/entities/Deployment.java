package entities;

public class Deployment {
	
	private double cov;
	private double cost; 
	
	public Deployment(double cov, double cost){
		this.cov=cov;
		this.cost=cost;
	}
	public double getCov() {
		return cov;
	}
	public void setCov(double cov) {
		this.cov = cov;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	

}
