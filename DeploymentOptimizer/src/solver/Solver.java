package solver;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import entities.Component;
import entities.Deployment;
import entities.Provider;

public class Solver {
	
	static double [][] criteria_goal_pref= new double [2][2];
	static double [][] cluster_cov_pref= new double [16][16];
	static double [][] cluster_cost_pref= new double [16][16];
	
	static double [] criteria_goal_priority;
	static double [] cluster_cov_priority;
	static double [] cluster_cost_priority;
	

	
	public static void main(String[] args) throws IOException {
		
		//lettura componenti
		ArrayList<Component> components=new ArrayList<Component>();
		ArrayList<Provider> providers=new ArrayList<Provider>();
		Component c;
		Provider p;
		
	
	   BufferedReader reader = new BufferedReader(new FileReader("components.txt"));
	 
	   String riga = reader.readLine();
	   String l=new String();
	   float [] values;
	   String [] controls;
	   
	   while(riga != null ){
		   String[] split = riga.split(";");
		   
		   c=new Component();
		   c.setID(split[0]);
		   c.setName(split[1]);
		   if(!split[2].equals("-1"))
			   c.setVMtype(Integer.valueOf(split[2]));//
		   if(!split[3].equals("-1"))
			   c.setStorageBlocks(Integer.valueOf(split[3]));
		   if(!split[4].equals("-1"))
			   c.setDataTransferRate(Integer.valueOf(split[4]));
		   
		   l=split[5];
		   String[] reqContr = l.split(",");
		   controls = new String [reqContr.length];
		   for(int i=0;i<reqContr.length;i++)
			   controls[i]=reqContr[i];
		   c.setRequiredControls(controls);
		   
		   
		   components.add(c);
		   
		   riga = reader.readLine();
	   }
	   
	   reader.close();
	   System.out.println("Componenti aggiunti:  "+components.size());
	   
	   
	   //lettura provider
	   reader = new BufferedReader(new FileReader("providers.txt"));
	   
	   riga = reader.readLine();
	   
	   while(riga != null ){
		   String[] split = riga.split(";");
		   p=new Provider();
		   p.setID(split[0]);
		   p.setName(split[1]);
		   
		   // VM costs
		   l=split[2];
		   String[] vmcosts = l.split(",");
		   values=new float[5];
		   for(int i=0;i<5;i++)
			   values[i]=Float.valueOf(vmcosts[i]);
		   p.setVMcosts(values);
		   
		   //storage block cost
		   p.setStorageBlockCost(Float.valueOf(split[3]));
		   
		   // data transfer rate costs
		   l=split[4];
		   String[] transfercosts = l.split(",");
		   values=new float[9];
		   for(int i=0;i<9;i++)
			   values[i]=Float.valueOf(transfercosts[i]);
		   p.setTransferRateCosts(values);
		   
		   
		   l=split[5];
		   String[] offContr = l.split(",");
		   controls = new String [offContr.length];
		   for(int i=0;i<offContr.length;i++)
			   controls[i]=offContr[i];
		   p.setOfferedControls(controls);
		   
		   
		   providers.add(p);
		   
		   riga = reader.readLine();
	   }
	   reader.close();
	   
	   System.out.println("Provider aggiunti:  "+providers.size());
	   
	   //inizializzo la matrice delle preferenze dei criteri rispetto al goal 
	   //e dei cluster rispetto ai criteri:
	   init_matrix();
	   
	   
	   Matrix criteria_goal;
	   Matrix clust_cov;
	   Matrix clust_cost;
		
	   criteria_goal=new Matrix(criteria_goal_pref);
	   clust_cov=new Matrix(cluster_cov_pref);
	   clust_cost=new Matrix(cluster_cost_pref);
		
	   //calcolo pesi relativi locali della gerarchia AHP
	   compute_local_priorities(criteria_goal, clust_cov, clust_cost);
	   
	   //generazione di una soluzione di deployment
	   int cov=0;
	   int cost=0;
	   
	   int k;
	   double w;
	   Deployment d;
	   FileWriter f;
	   f=new FileWriter("experiments.txt");
	    
	    
	   while(cov<100){
		   cost=0;
		   while(cost<100){
			   d=new Deployment(cov,cost);
			   k=identify_cluster(d);
			   //System.out.print("Cluster "+k+" (Cov="+cov+", Cost="+cost+")");
			   w=(100-cost)*cluster_cost_priority[k]*criteria_goal_priority[1]+
					   cov*cluster_cov_priority[k]*criteria_goal_priority[0];
			   System.out.println("  W= "+w);
			   f.write(k+";"+cov+";"+cost+";"+w+"\n");
			   
			   
			   cost+=5;
		   }
		   cov+=5;
	   }
	   
	  f.flush();
	  f.close();
	   
	   
	}
	
	
	static void init_matrix(){
		
		//matrix of preferences criteria vs goal
		//le righe sono cov e cost rispettivamente
		criteria_goal_pref [0][0]=1;
		criteria_goal_pref [1][1]=1;
		criteria_goal_pref [0][1]=7;
		criteria_goal_pref [1][0]=1.0/7.0;
		
		System.out.println("Preference matrix rispetto al goal: ");
		show_matrix(criteria_goal_pref);
		
		
		//cluster_cov matrix
		double [] prefs={3,5,7};
		
		for(int i=0;i<16;i++){
			for(int j=0;j<16;j++){
				if((j-i)%4==0)
					cluster_cov_pref[i][j]=1;
				else if((i%4-j%4)==1){
					cluster_cov_pref[i][j]=prefs[0];
					cluster_cov_pref[j][i]=1/prefs[0];
				}
				else if((i%4-j%4)==2){
					cluster_cov_pref[i][j]=prefs[1];
					cluster_cov_pref[j][i]=1/prefs[1];
				}
				else if((i%4-j%4)==3){
					cluster_cov_pref[i][j]=prefs[2];
					cluster_cov_pref[j][i]=1/prefs[2];
				}
			}
		}
		System.out.println("Preference matrix rispetto alla coverage: ");
		show_matrix(cluster_cov_pref);
		
		
		
		//cluster_cost matrix		
		for(int i=0;i<16;i++){
			for(int j=0;j<16;j++){
				if(i>=0&&i<4&&j>=0&&j<4 || i>=4&&i<8&&j>=4&&j<8 || i>=8&&i<12&&j>=8&&j<12 || i>=12&&i<16&&j>=12&&j<16)
					cluster_cost_pref[i][j]=1;
				else if(i>=0&&i<4&&j>=4&&j<8 || i>=4&&i<8&&j>=8&&j<12 || i>=8&&i<12&&j>=12&&j<16){
					cluster_cost_pref[i][j]=prefs[0];
					cluster_cost_pref[j][i]=1/prefs[0];
				}
				else if(i>=0&&i<4&&j>=8&&j<12 || i>=4&&i<8&&j>=12&&j<16){
					cluster_cost_pref[i][j]=prefs[1];
					cluster_cost_pref[j][i]=1/prefs[1];
				}
				else if(i>=0&&i<4&&j>=12&&j<16){
					cluster_cost_pref[i][j]=prefs[2];
					cluster_cost_pref[j][i]=1/prefs[2];
				}
			}
		}
		System.out.println("Preference matrix rispetto al cost: ");
		show_matrix(cluster_cost_pref);

		
	}
	
	public static void show_matrix(double[][] b )
	{
		//display the elements of the matrix a
		//System.out.println("\nMatrix :");
		for(int i=0; i<b.length;i++)
		{
			for(int j=0; j<b[i].length; j++)
				System.out.print(b[i][j]+"    ");
			System.out.println();   
		}
		System.out.println();  
	}
	
	public static void compute_local_priorities(Matrix critgoal, Matrix clustCov, Matrix clustCost){
		
		//questo metodo calcola i pesi locali a ciascun livello:
		//1) livello criteri (cov/cost rispetto a goal)
		//2) livello cluster (cluster rispetto a cov/cluster rispetto a cost)
		
		/******************************************************************/
		
		//AHP ottiene i pesi delle alternative dall'autovettore principale 
		//della matrice delle preferenze. L'autovattore principale � quello
		//a cui corrisponde l'autovalore massimo
		//Se A � la matrice NxN di partenza, e v un vettore di N elementi non nullo,
		//gli autovalori di A sono tutti quei valori l tali che:
		//Av=lv   (v � l'autovettore destro di A)
		
		
		/******************************************************************/
		/******************* PESI LIVELLO CRITERI vs GOAL *****************/
		
		criteria_goal_priority=new double[2]; //vettore dei pesi di cov e cost
										   //rispetto a goal
		//criteria_goal_priority[0] � il peso di coverage,
		//criteria_goal_priority[1] � il peso di cost
		
		
		EigenvalueDecomposition dec_goal=new EigenvalueDecomposition(critgoal);
		double[] autovals_goal=dec_goal.getRealEigenvalues();
		int maxpos=0;
		
		for(int i=0;i<autovals_goal.length;i++){
			if(autovals_goal[i]>autovals_goal[maxpos])
				maxpos=i;
		}
		Matrix autovect_goal_mat=dec_goal.getV();
		//inserisce in pesiVSgoal gli elementi dell'autovettore principale:		
		//System.out.println("\nAutovettore principale non normalizzato:");
		for(int i=0;i<criteria_goal_priority.length;i++){
			criteria_goal_priority[i]=autovect_goal_mat.get(i,maxpos); //prende la maxpos-esima colonna della matrice
			//System.out.print(pesiVSgoal[i]+"  ");
		}
		System.out.println();
		double minimo=criteria_goal_priority[0], massimo=criteria_goal_priority[0];
		
		for(int i=0;i<criteria_goal_priority.length;i++){
			if(criteria_goal_priority[i]>massimo){
				massimo=criteria_goal_priority[i];
			}
			if(criteria_goal_priority[i]<minimo){
				minimo=criteria_goal_priority[i];
			}
		}
		//System.out.println("\nMax pesiVSgoal �: "+massimo);
		//System.out.println("Min pesiVSgoal �: "+minimo);
		double interval=massimo-minimo;
		//System.out.println("Intervallo= "+interval);
		double sum = 0.0; 
				
		//NORMALIZZAZIONE
		//calcolo la somma dei valori dell'autovettore:
		for (int i = 0; i < criteria_goal_priority.length; i++) { 
			sum += criteria_goal_priority[i]; 
		}
		
		//divido ogni numero per la somma in modo che la somma faccia 1
		System.out.println("\nVettore dei pesi di cov e cost rispetto a goal:");
		for (int i = 0; i < criteria_goal_priority.length; i++) { 
			criteria_goal_priority[i] = criteria_goal_priority[i]/sum; 
			System.out.print(criteria_goal_priority[i]+"  ");
		} 
		System.out.println();
		
		/*************************************************************/
		/******************* PESI dei 16 CLUSTER vs COV *****************/
			
		
		//vettore che contiene 16 elementi e conterr� l'autovettore 
		//principale normalizzato (in modo che la somma dei suoi valori sia 1)
		
		cluster_cov_priority=new double[16];
		
		//calcola la parte reale degli autovalori della matrice delle preferenze
		//dei cluster rispetto alla coverage (A)
		EigenvalueDecomposition dec=new EigenvalueDecomposition(clustCov);
		double[] values=dec.getRealEigenvalues();
		
		//calcola il massimo degli autovalori
		maxpos=0;
		for(int i=0;i<values.length;i++){
			if(values[i]>values[maxpos])
				maxpos=i;
		}
		//System.out.println("Max autovalore �: "+values[maxpos]);
		
		//preleva l'autovettore principale dalla matrice degli autovettori
		//cio� quello che corrisponde all'autovalore maggiore
		
		//matrice di tutti gli autovettori:
		Matrix autovettori_mat=dec.getV();
		//inserisce in ret gli elementi dell'autovettore principale:		
		//System.out.println("Autovettore principale non normalizzato:");
		for(int i=0;i<cluster_cov_priority.length;i++){
			cluster_cov_priority[i]=autovettori_mat.get(i,maxpos); //prende la maxpos-esima colonna della matrice
			//System.out.print(ret[i]+"  ");
		}
		//adesso devo normalizzare questo autovettore principale:
		
		//calcola min e max di ret
		
		minimo=cluster_cov_priority[0];
		massimo=cluster_cov_priority[0];
				
		for(int i=0;i<cluster_cov_priority.length;i++){
			if(cluster_cov_priority[i]>massimo){
				massimo=cluster_cov_priority[i];
			}
			if(cluster_cov_priority[i]<minimo){
				minimo=cluster_cov_priority[i];
			}
		}
		//System.out.println("\nMax ret �: "+massimo);
		//System.out.println("Min ret �: "+minimo);
		interval=massimo-minimo;
		//System.out.println("Intervallo= "+interval);
		sum = 0.0; 
		
		
		//normalizzazione alternativa, prima fra 0 e 1 e poi sulla somma:
		//questa � quella adottata da jacopo che per� non si trova con 
		//i valori da noi ricavati con excel:
//				System.out.println("Ret normalizzato fra 0 e 1:");
//				for (int i = 0; i < cluster_cov_priority.length; i++) { 
//					cluster_cov_priority[i] = (cluster_cov_priority[i] - minimo)/interval; 
//					System.out.print(cluster_cov_priority[i]+"  ");
//					sum += cluster_cov_priority[i]; 
//				}
		
		//NORMALIZZAZIONE
		//calcolo la somma dei valori dell'autovettore:
		for (int i = 0; i < cluster_cov_priority.length; i++) { 
			sum += cluster_cov_priority[i]; 
		}
		
		//divido ogni numero per la somma in modo che la somma faccia 1
		System.out.println("\nVettore dei pesi dei cluster rispetto a coverage:");
		for (int i = 0; i < cluster_cov_priority.length; i++) { 
			cluster_cov_priority[i] = cluster_cov_priority[i]/sum; 
			System.out.print(cluster_cov_priority[i]+"  ");
		} 
		
		//prova:
//		sum=0.0;
//		for(int i=0;i<ret.length;i++)
//			sum+=ret[i];
//		System.out.println("\nSomma: "+sum);
		
		System.out.println();
		/*************************************************************/
		/******************* PESI dei 16 CLUSTER vs COST *****************/
			
		
		// vettore che contiene 16 elementi e conterr� l'autovettore 
		//principale normalizzato (in modo che la somma dei suoi valori sia 1)

		cluster_cost_priority=new double[16];
		
		
		//calcola la parte reale degli autovalori della matrice delle preferenze
		//dei cluster rispetto alla coverage (A)
		EigenvalueDecomposition dec1=new EigenvalueDecomposition(clustCost);
		double[] values1=dec1.getRealEigenvalues();
		
		//calcola il massimo degli autovalori
		maxpos=0;
		for(int i=0;i<values1.length;i++){
			if(values1[i]>values1[maxpos])
				maxpos=i;
		}
		//System.out.println("Max autovalore �: "+values1[maxpos]);
		
		//preleva l'autovettore principale dalla matrice degli autovettori
		//cio� quello che corrisponde all'autovalore maggiore
		
		//matrice di tutti gli autovettori:
		Matrix autovettori_mat1=dec1.getV();
		//inserisce in ret gli elementi dell'autovettore principale:		
		//System.out.println("Autovettore principale non normalizzato:");
		for(int i=0;i<cluster_cost_priority.length;i++){
			cluster_cost_priority[i]=autovettori_mat1.get(i,maxpos); //prende la maxpos-esima colonna della matrice
			//System.out.print(cluster_cost_priority[i]+"  ");
		}
		//adesso devo normalizzare questo autovettore principale:
		
		//calcola min e max di ret
		
		minimo=cluster_cost_priority[0];
		massimo=cluster_cost_priority[0];
				
		for(int i=0;i<cluster_cost_priority.length;i++){
			if(cluster_cost_priority[i]>massimo){
				massimo=cluster_cost_priority[i];
			}
			if(cluster_cost_priority[i]<minimo){
				minimo=cluster_cost_priority[i];
			}
		}
		//System.out.println("\nMax ret �: "+massimo);
		//System.out.println("Min ret �: "+minimo);
		interval=massimo-minimo;
		//System.out.println("Intervallo= "+interval);
		sum = 0.0; 
		
		
		
		
		//NORMALIZZAZIONE
		//calcolo la somma dei valori dell'autovettore:
		for (int i = 0; i < cluster_cost_priority.length; i++) { 
			sum += cluster_cost_priority[i]; 
		}
		
		//divido ogni numero per la somma in modo che la somma faccia 1
		System.out.println("\nVettore dei pesi dei cluster rispetto a cost: ");
		for (int i = 0; i < cluster_cost_priority.length; i++) { 
			cluster_cost_priority[i] = cluster_cost_priority[i]/sum; 
			System.out.print(cluster_cost_priority[i]+"  ");
		} 
		System.out.println();
		//prova:
//		sum=0.0;
//		for(int i=0;i<ret.length;i++)
//			sum+=ret[i];
//		System.out.println("\nSomma: "+sum);
		
		
		
		
		/***************************************************** */
		//prova: provo che quello che � uscito � davvero l'autovettore di A
		//cio� che A*v=l*v
		//dove A=cluster_cov_pref, v=ret (autovettore), l=max autoval
//		System.out.println("\n-----------PROVA--------------");
//		
//		double[] v1=new double[16];
//		double[] v2=new double[16];
//		
//		//A*v=cluster_cov_pref*ret
//		for(int i=0;i<16;i++){
//			v1[i]=0;
//			for(int j=0;j<16;j++){
//				v1[i]=v1[i]+cluster_cov_pref[i][j]*ret[j];
//			}
//			System.out.print(v1[i]+"  "); 
//		}
//		System.out.println();
//		//l*v=max_autoval*ret
//		for(int i=0;i<16;i++){
//			v2[i]=ret[i]*values[maxpos];
//			System.out.print(v2[i]+"  "); 
//		}
//		System.out.println();
		/******************************************************************/
		
		
			
	}
	
	public static int identify_cluster(Deployment d){
		//ci sono 16 cluster
		int k=-1;
		double cov=d.getCov();
		double cost=d.getCost();
		
		if(cost >=0&&cost<=25){//primo pedice=1
			if(cov >=0 && cov<=25)
				k=0;
			else if(cov >25 && cov<=50)
				k=1;
			else if(cov >5 && cov<=75)
				k=2;
			else if(cov >75 && cov<=100)
				k=3;
		}
		if(cost >25 && cost<=50){//primo pedice=2
			if(cov >=0 && cov<=25)
				k=4;
			else if(cov >25 && cov<=50)
				k=5;
			else if(cov >50 && cov<=75)
				k=6;
			else if(cov >75 && cov<=100)
				k=7;
		}
		if(cost >50 && cost<=75){//primo pedice=3
			if(cov >=0 && cov<=25)
				k=8;
			else if(cov >25 && cov<=50)
				k=9;
			else if(cov >50 && cov<=75)
				k=10;
			else if(cov >75 && cov<=100)
				k=11;
		}
		if(cost >75 && cost<=100){//primo pedice=4
			if(cov >=0 && cov<=25)
				k=12;
			else if(cov >25 && cov<=50)
				k=13;
			else if(cov >50 && cov<=75)
				k=14;
			else if(cov >75 && cov<=100)
				k=15;
		}
		return k;
		
	}
	   

}
